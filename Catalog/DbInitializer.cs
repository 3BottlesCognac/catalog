﻿using System.Collections.Generic;
using System.Data.Entity;
using Catalog.Entity;
using Catalog.Entity.Books;
using Catalog.Enums;

namespace Catalog
{
    public class DbInitializer : DropCreateDatabaseAlways<DBContext>
    {
        protected override void Seed(DBContext context)
        {
            var programmings = new List<Programming>
            {
                new Programming
                {
                    Name = "book1",
                    Code = "123qwert",
                    Cost = "123",
                    NumberPages = 1000,
                    Language = "lang1",
                },
                new Programming
                {
                    Name = "book2",
                    Code = "123qwert",
                    Cost = "123",
                    NumberPages = 1000,
                    Language = "lang2",
                },
                new Programming
                {
                    Name = "book3",
                    Code = "123qwert",
                    Cost = "123",
                    NumberPages = 1000,
                    Language = "lang3",
                }
            };

            var disks = new List<Disk>
            {
                new Disk
                {
                    Code = "musicDvd1",
                    Content = EDickContent.Music,
                    Cost = "123",
                    Name = "musicDvd1",
                    TypeDisk = EDisk.DVD
                },
                new Disk
                {
                    Code = "SoftwareDVD1",
                    Content = EDickContent.Software,
                    Cost = "123123",
                    Name = "SoftwareDVD1",
                    TypeDisk = EDisk.DVD
                },
                new Disk
                {
                    Code = "VideoDVD1",
                    Content = EDickContent.Video,
                    Cost = "123123",
                    Name = "VideoDVD1",
                    TypeDisk = EDisk.DVD
                },
                new Disk
                {
                    Code = "VideoDVD2",
                    Content = EDickContent.Video,
                    Cost = "123123",
                    Name = "VideoDVD2",
                    TypeDisk = EDisk.DVD
                },
                new Disk
                {
                    Code = "MusicCD1",
                    Content = EDickContent.Music,
                    Cost = "qwe",
                    Name = "MusicCD1",
                    TypeDisk = EDisk.CD
                },
                new Disk
                {
                    Code = "MusicCD2",
                    Content = EDickContent.Music,
                    Cost = "123",
                    Name = "MusicCD2",
                    TypeDisk = EDisk.CD
                },
                new Disk
                {
                    Code = "SoftwareCD1",
                    Content = EDickContent.Software,
                    Cost = "qwe",
                    Name = "SoftwareCD1",
                    TypeDisk = EDisk.CD
                },
                new Disk
                {
                    Code = "VideoCD1",
                    Content = EDickContent.Video,
                    Cost = "qwe",
                    Name = "VideoCD1",
                    TypeDisk = EDisk.CD
                }
            };

            var cooking = new List<Cooking>()
            {
                new Cooking
                {
                    Name = "book1",
                    Code = "123qwert",
                    Cost = "123",
                    NumberPages = 1000,
                    BasicIngredient = "book1"
                },
                new Cooking
                {
                    Name = "book2",
                    Code = "123qwert",
                    Cost = "123",
                    NumberPages = 1000,
                    BasicIngredient = "book2"
                },
                new Cooking
                {
                    Name = "book3",
                    Code = "123qwert",
                    Cost = "123",
                    NumberPages = 1000,
                    BasicIngredient = "book3"
                }
            };
            
            var esoterics = new List<Esoterics>()
            {
                new Esoterics
                {
                    Name = "book1",
                    Code = "123qwert",
                    Cost = "123",
                    NumberPages = 1000,
                    MinAge = 25
                },
                new Esoterics
                {
                    Name = "book2",
                    Code = "123qwert",
                    Cost = "123",
                    NumberPages = 1000,
                    MinAge = 25
                },
                new Esoterics
                {
                    Name = "book3",
                    Code = "123qwert",
                    Cost = "123",
                    NumberPages = 1000,
                    MinAge = 25
                }
            };

            context.Set<Programming>().AddRange(programmings);
            context.Set<Cooking>().AddRange(cooking);
            context.Set<Esoterics>().AddRange(esoterics);
            context.Set<Disk>().AddRange(disks);
            base.Seed(context);
        }
    }
}