﻿using Catalog.DomainServices.Interfaces;
using Catalog.Entity;

namespace Catalog.DomainServices
{
    public sealed class DiskDomainService : BaseDomainService<Disk>, IDiskDomainService
    {
    }
}