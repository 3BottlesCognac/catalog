﻿using Backpack.DomainServices.Interfaces;
using Catalog.Entity.Books;

namespace Catalog.DomainServices.Interfaces.Books
{
    public interface IProgrammingDomainService : IBaseDomainService<Programming>
    {
    }
}