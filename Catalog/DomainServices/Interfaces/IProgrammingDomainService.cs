﻿using Backpack.DomainServices.Interfaces;
using Catalog.Entity;

namespace Catalog.DomainServices.Interfaces
{
    public interface IDiskDomainService : IBaseDomainService<Disk>
    {
    }
}