﻿using System;
using System.Linq;

namespace Backpack.DomainServices.Interfaces
{
    public interface IBaseDomainService<TEntity> : IDisposable where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        void Save(TEntity entity);
        void Save(params TEntity[] entities);
        TEntity Get(long id);
        void Remove(long id);
        void Edit(TEntity entity);
        void Edit(params TEntity[] entities);
    }
}