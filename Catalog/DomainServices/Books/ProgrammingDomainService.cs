﻿using Catalog.DomainServices.Interfaces.Books;
using Catalog.Entity.Books;

namespace Catalog.DomainServices
{
    public sealed class ProgrammingDomainService : BaseDomainService<Programming>, IProgrammingDomainService
    {
    }
}