﻿using Catalog.DomainServices.Interfaces.Books;
using Catalog.Entity.Books;

namespace Catalog.DomainServices
{
    public sealed class CookingDomainService : BaseDomainService<Cooking>, ICookingDomainService
    {
    }
}