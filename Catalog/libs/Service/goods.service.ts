﻿import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import {Subject} from "rxjs/Subject";

@Injectable()
export class GoodsService {
    private diskSubject = new Subject<number>();
    private disks: number;
    disks$ = this.diskSubject.asObservable();

    addTypeDisk(data: number) {
        this.diskSubject.next(data);
        this.disks = data;
    }
    
    getDisks():number{
        return this.disks;
    }

    constructor(private _http: Http) {
    }

    get(url: string): Observable<any> {
        return this._http.get(url)
            .map((response: Response) => response.json() as any)
            .catch(this.handleError);
    }

    post(url: string, model: any): Observable<any> {
        const body = JSON.stringify(model);
        const headers = new Headers({ 'Content-Type': "application/json" });
        const options = new RequestOptions({ headers: headers });
        return this._http.post(url, body, options)
            .map((response: Response) => response.json() as any)
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.json().error || "Server error");
    }

}