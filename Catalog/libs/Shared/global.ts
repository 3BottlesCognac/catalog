export class Global {

    static pathToView(filename: string, dirName?: string): string {
        let pathToView = "libs/pages/views/";
        if (dirName) {
            pathToView += dirName + "/";
        }
        return pathToView + filename + ".component.html";
    }
}