"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = /** @class */ (function () {
    function Global() {
    }
    Global.pathToView = function (filename, dirName) {
        var pathToView = "libs/pages/views/";
        if (dirName) {
            pathToView += dirName + "/";
        }
        return pathToView + filename + ".component.html";
    };
    return Global;
}());
exports.Global = Global;
//# sourceMappingURL=global.js.map