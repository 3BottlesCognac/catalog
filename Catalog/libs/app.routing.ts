import {Routes, RouterModule} from "@angular/router";
import {NotFoundComponent} from "./Pages/Components/not-found.component";
import {HomeComponent} from "./Pages/Components/home.component";
import {BooksComponent} from "./Pages/Components/Books/books.component";
import {ProgrammingComponent} from "./Pages/Components/Books/programming.component";
import {CookingComponent} from "./Pages/Components/Books/cooking.component";
import {EsotericsComponent} from "./Pages/Components/Books/esoterics.component";
import {CdComponent} from "./Pages/Components/Disks/cd.component";
import {VideoComponent} from "./Pages/Components/Disks/video.component";
import {SoftwareComponent} from "./Pages/Components/Disks/software.component";
import {MusicComponent} from "./Pages/Components/Disks/music.component";
import {DvdComponent} from "./Pages/Components/Disks/dvd.component";

const appRoutes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: HomeComponent },
    {
        path: "books",
        component: BooksComponent,
        children: [
            { path: "", redirectTo: "programming", pathMatch: "full" },
            { path: "programming", component: ProgrammingComponent },
            { path: "cooking", component: CookingComponent },
            { path: "esoterics", component: EsotericsComponent }
        ]
    },
    {
        path: "cd",
        component: CdComponent,
        children: [
            { path: "", redirectTo: "video", pathMatch: "full" },
            { path: "video", component: VideoComponent },
            { path: "software", component: SoftwareComponent },
            { path: "music", component: MusicComponent }
        ]
    },
    {
        path: "dvd",
        component: DvdComponent,
        children: [
            { path: "", redirectTo: "video", pathMatch: "full" },
            { path: "video", component: VideoComponent },
            { path: "software", component: SoftwareComponent },
            { path: "music", component: MusicComponent }
        ]
    },
    { path: "**", component: NotFoundComponent }
];

export const routing =
    RouterModule.forRoot(appRoutes);