"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var not_found_component_1 = require("./Pages/Components/not-found.component");
var home_component_1 = require("./Pages/Components/home.component");
var books_component_1 = require("./Pages/Components/Books/books.component");
var programming_component_1 = require("./Pages/Components/Books/programming.component");
var cooking_component_1 = require("./Pages/Components/Books/cooking.component");
var esoterics_component_1 = require("./Pages/Components/Books/esoterics.component");
var cd_component_1 = require("./Pages/Components/Disks/cd.component");
var video_component_1 = require("./Pages/Components/Disks/video.component");
var software_component_1 = require("./Pages/Components/Disks/software.component");
var music_component_1 = require("./Pages/Components/Disks/music.component");
var dvd_component_1 = require("./Pages/Components/Disks/dvd.component");
var appRoutes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: home_component_1.HomeComponent },
    {
        path: "books",
        component: books_component_1.BooksComponent,
        children: [
            { path: "", redirectTo: "programming", pathMatch: "full" },
            { path: "programming", component: programming_component_1.ProgrammingComponent },
            { path: "cooking", component: cooking_component_1.CookingComponent },
            { path: "esoterics", component: esoterics_component_1.EsotericsComponent }
        ]
    },
    {
        path: "cd",
        component: cd_component_1.CdComponent,
        children: [
            { path: "", redirectTo: "video", pathMatch: "full" },
            { path: "video", component: video_component_1.VideoComponent },
            { path: "software", component: software_component_1.SoftwareComponent },
            { path: "music", component: music_component_1.MusicComponent }
        ]
    },
    {
        path: "dvd",
        component: dvd_component_1.DvdComponent,
        children: [
            { path: "", redirectTo: "video", pathMatch: "full" },
            { path: "video", component: video_component_1.VideoComponent },
            { path: "software", component: software_component_1.SoftwareComponent },
            { path: "music", component: music_component_1.MusicComponent }
        ]
    },
    { path: "**", component: not_found_component_1.NotFoundComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map