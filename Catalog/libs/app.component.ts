﻿import {Component} from "@angular/core";
import {Global} from "./Shared/global";

@Component({
    selector: "angular",
    templateUrl: Global.pathToView("app")
})
export class AppComponent {
}