﻿import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {NotFoundComponent} from "./Pages/Components/not-found.component";
import {CommonModule} from "@angular/common";
import {HomeComponent} from "./Pages/Components/home.component";
import {routing} from "./app.routing";
import {NavComponent} from "./Pages/Components/nav.component";
import {BooksComponent} from "./Pages/Components/Books/books.component";
import {CookingComponent} from "./Pages/Components/Books/cooking.component";
import {ProgrammingComponent} from "./Pages/Components/Books/programming.component";
import {EsotericsComponent} from "./Pages/Components/Books/esoterics.component";
import {GoodsService} from "./Service/goods.service";
import {CdComponent} from "./Pages/Components/Disks/cd.component";
import {VideoComponent} from "./Pages/Components/Disks/video.component";
import {SoftwareComponent} from "./Pages/Components/Disks/software.component";
import {MusicComponent} from "./Pages/Components/Disks/music.component";
import {DvdComponent} from "./Pages/Components/Disks/dvd.component";
import {BaseDiskComponent} from "./Pages/Components/Disks/base-disk.component";

@NgModule({
    imports: [BrowserModule, HttpModule, routing, CommonModule],
    declarations: [
        AppComponent, HomeComponent, NotFoundComponent, NavComponent, BooksComponent, CookingComponent,
        ProgrammingComponent, EsotericsComponent, CdComponent, DvdComponent, MusicComponent, VideoComponent,
        SoftwareComponent, BaseDiskComponent
    ],
    providers: [GoodsService],
    bootstrap: [AppComponent]
})
export class AppModule {
}