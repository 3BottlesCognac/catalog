"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var app_component_1 = require("./app.component");
var not_found_component_1 = require("./Pages/Components/not-found.component");
var common_1 = require("@angular/common");
var home_component_1 = require("./Pages/Components/home.component");
var app_routing_1 = require("./app.routing");
var nav_component_1 = require("./Pages/Components/nav.component");
var books_component_1 = require("./Pages/Components/Books/books.component");
var cooking_component_1 = require("./Pages/Components/Books/cooking.component");
var programming_component_1 = require("./Pages/Components/Books/programming.component");
var esoterics_component_1 = require("./Pages/Components/Books/esoterics.component");
var goods_service_1 = require("./Service/goods.service");
var cd_component_1 = require("./Pages/Components/Disks/cd.component");
var video_component_1 = require("./Pages/Components/Disks/video.component");
var software_component_1 = require("./Pages/Components/Disks/software.component");
var music_component_1 = require("./Pages/Components/Disks/music.component");
var dvd_component_1 = require("./Pages/Components/Disks/dvd.component");
var base_disk_component_1 = require("./Pages/Components/Disks/base-disk.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, http_1.HttpModule, app_routing_1.routing, common_1.CommonModule],
            declarations: [
                app_component_1.AppComponent, home_component_1.HomeComponent, not_found_component_1.NotFoundComponent, nav_component_1.NavComponent, books_component_1.BooksComponent, cooking_component_1.CookingComponent,
                programming_component_1.ProgrammingComponent, esoterics_component_1.EsotericsComponent, cd_component_1.CdComponent, dvd_component_1.DvdComponent, music_component_1.MusicComponent, video_component_1.VideoComponent,
                software_component_1.SoftwareComponent, base_disk_component_1.BaseDiskComponent
            ],
            providers: [goods_service_1.GoodsService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map