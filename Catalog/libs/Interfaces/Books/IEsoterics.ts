import {IBaseBook} from "./IBaseBook";

export interface IEsoterics extends IBaseBook {
    MinAge: number;
}