import {IBaseBook} from "./IBaseBook";

export interface IProgramming extends IBaseBook {
    Language: string;
}