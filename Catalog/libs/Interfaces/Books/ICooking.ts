import {IBaseBook} from "./IBaseBook";

export interface ICooking extends IBaseBook {
    BasicIngredient: string;
}