import {IBaseGoods} from "../IBaseGoods";

export interface IBaseBook extends IBaseGoods {
    NumberPages: number;
}