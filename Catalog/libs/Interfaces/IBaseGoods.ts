export interface IBaseGoods {
    Name: string;
    Cost: number;
    Code: string;
}