﻿import {Component, OnInit} from "@angular/core";
import {IProgramming} from "../../../Interfaces/Books/IProgramming";
import {GoodsService} from "../../../Service/goods.service";
import {Global} from "../../../Shared/global";

@Component({
    templateUrl: Global.pathToView("programming", "books")
})
export class ProgrammingComponent implements OnInit {

    goods: IProgramming[];

    constructor(private goodService: GoodsService) {
    }

    ngOnInit() {
        this.goodService.get("programming/List").subscribe(data => {
            this.goods = data;
        });
    }

}