﻿import {Component, OnInit} from "@angular/core";
import {Global} from "../../../Shared/global";
import {GoodsService} from "../../../Service/goods.service";
import {ICooking} from "../../../Interfaces/Books/ICooking";

@Component({
    templateUrl: Global.pathToView("cooking", "books")
})
export class CookingComponent implements OnInit {
    goods: ICooking[];

    constructor(private goodService: GoodsService) {
    }

    ngOnInit() {
        this.goodService.get("cooking/List").subscribe(data => {
            this.goods = data;
        });
    }
}