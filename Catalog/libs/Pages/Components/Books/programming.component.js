"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var goods_service_1 = require("../../../Service/goods.service");
var global_1 = require("../../../Shared/global");
var ProgrammingComponent = /** @class */ (function () {
    function ProgrammingComponent(goodService) {
        this.goodService = goodService;
    }
    ProgrammingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.goodService.get("programming/List").subscribe(function (data) {
            _this.goods = data;
        });
    };
    ProgrammingComponent = __decorate([
        core_1.Component({
            templateUrl: global_1.Global.pathToView("programming", "books")
        }),
        __metadata("design:paramtypes", [goods_service_1.GoodsService])
    ], ProgrammingComponent);
    return ProgrammingComponent;
}());
exports.ProgrammingComponent = ProgrammingComponent;
//# sourceMappingURL=programming.component.js.map