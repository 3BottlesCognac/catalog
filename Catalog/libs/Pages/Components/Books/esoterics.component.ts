﻿import {Component, OnInit} from "@angular/core";
import {Global} from "../../../Shared/global";
import {GoodsService} from "../../../Service/goods.service";
import {IEsoterics} from "../../../Interfaces/Books/IEsoterics";

@Component({
    templateUrl: Global.pathToView("esoterics", "books")
})
export class EsotericsComponent implements OnInit {
    goods: IEsoterics[];

    constructor(private goodService: GoodsService) {
    }

    ngOnInit() {
        this.goodService.get("esoterics/List").subscribe(data => {
            this.goods = data;
        });
    }
}