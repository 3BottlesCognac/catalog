﻿import {Component} from "@angular/core";
import { Router } from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Http, Response} from "@angular/http";
import "rxjs/add/operator/map";

@Component({
    template: "<h2>Ты там, где ты</h2>"
})
export class HomeComponent {

    constructor(private router: Router, private http: Http) {
    }

    private get(): Observable<any> {
        return this.http.get("home/ListNavigation").map((response: Response) => response.json() as any);
    }
}