﻿import {Component, OnInit} from "@angular/core";
import {GoodsService} from "../../../Service/goods.service";

@Component({
    template: "<router-outlet></router-outlet>"
})
export class DvdComponent implements OnInit {
    typeDisk = 2;

    constructor(private goodService: GoodsService) {
    }

    ngOnInit() {
        this.goodService.addTypeDisk(this.typeDisk);
    };
}