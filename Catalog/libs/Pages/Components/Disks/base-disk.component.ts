﻿import {Component, Input, OnInit} from "@angular/core";
import {Global} from "../../../Shared/global";
import {IDisk} from "../../../Interfaces/IDisk";
import {GoodsService} from "../../../Service/goods.service";

@Component({
    selector: "disk",
    templateUrl: Global.pathToView("base-disk", "disks")
})
export class BaseDiskComponent implements OnInit {
    goods: IDisk[];
    @Input()
    content: number;
    typeDisk: number;

    constructor(private goodService: GoodsService) {
        goodService.disks$.subscribe(data => this.typeDisk = data);
    }

    ngOnInit() {

        if (!this.typeDisk) {
            this.typeDisk = this.goodService.getDisks();
        }
        const params = {content: this.content, typeDisk: this.typeDisk};
        this.goodService.post("disk/List", params).subscribe(data => {
            this.goods = data;
        });
    }
}