﻿import {Component, OnInit} from "@angular/core";
import {GoodsService} from "../../../Service/goods.service";

@Component({
    template: "<router-outlet></router-outlet>"
})
export class CdComponent implements OnInit {
    typeDisk = 1;

    constructor(private goodService: GoodsService) {
    }

    ngOnInit() {
        this.goodService.addTypeDisk(this.typeDisk);
    };
}