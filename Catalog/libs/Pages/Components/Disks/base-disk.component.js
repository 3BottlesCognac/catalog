"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var global_1 = require("../../../Shared/global");
var goods_service_1 = require("../../../Service/goods.service");
var BaseDiskComponent = /** @class */ (function () {
    function BaseDiskComponent(goodService) {
        var _this = this;
        this.goodService = goodService;
        goodService.disks$.subscribe(function (data) { return _this.typeDisk = data; });
    }
    BaseDiskComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.typeDisk) {
            this.typeDisk = this.goodService.getDisks();
        }
        var params = { content: this.content, typeDisk: this.typeDisk };
        this.goodService.post("disk/List", params).subscribe(function (data) {
            _this.goods = data;
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], BaseDiskComponent.prototype, "content", void 0);
    BaseDiskComponent = __decorate([
        core_1.Component({
            selector: "disk",
            templateUrl: global_1.Global.pathToView("base-disk", "disks")
        }),
        __metadata("design:paramtypes", [goods_service_1.GoodsService])
    ], BaseDiskComponent);
    return BaseDiskComponent;
}());
exports.BaseDiskComponent = BaseDiskComponent;
//# sourceMappingURL=base-disk.component.js.map