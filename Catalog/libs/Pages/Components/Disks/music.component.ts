﻿import {Component} from "@angular/core";

@Component({
    template: '<disk [content]="content"></disk>'
})
export class MusicComponent {
    content = 5;
}