﻿import {AfterViewInit, Component} from "@angular/core";
import {Global} from "../../Shared/global";
declare var $: any;

@Component({
    selector: "nav",
    templateUrl: Global.pathToView("nav")
})
export class NavComponent implements AfterViewInit {

    ngAfterViewInit() {
        $(".tree-toggle").click(function() {
            $(this).parent().children("ul.tree").toggle(200);
        });
    }
}