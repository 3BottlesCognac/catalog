﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {HomeComponent} from "../Components/home.component";

@NgModule({
    imports: [CommonModule],
    declarations: [HomeComponent],
    bootstrap: [HomeComponent]
})
export class HomeModule {
}