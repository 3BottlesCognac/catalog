﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {EsotericsComponent} from "../../Components/Books/esoterics.component";

@NgModule({
    imports: [CommonModule],
    declarations: [EsotericsComponent],
    bootstrap: [EsotericsComponent]
})
export class EsotericsModule {
}