"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var cooking_component_1 = require("../../Components/Books/cooking.component");
var esoterics_component_1 = require("../../Components/Books/esoterics.component");
var programming_component_1 = require("../../Components/Books/programming.component");
var books_component_1 = require("../../Components/Books/books.component");
var BooksModule = /** @class */ (function () {
    function BooksModule() {
    }
    BooksModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule],
            declarations: [books_component_1.BooksComponent, cooking_component_1.CookingComponent, esoterics_component_1.EsotericsComponent, programming_component_1.ProgrammingComponent],
            bootstrap: [books_component_1.BooksComponent]
        })
    ], BooksModule);
    return BooksModule;
}());
exports.BooksModule = BooksModule;
//# sourceMappingURL=books.module.js.map