﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ProgrammingComponent} from "../../Components/Books/programming.component";

@NgModule({
    imports: [CommonModule],
    declarations: [ProgrammingComponent],
    bootstrap: [ProgrammingComponent]
})
export class ProgrammingModule {
}