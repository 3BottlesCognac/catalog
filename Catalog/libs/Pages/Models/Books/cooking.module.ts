﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CookingComponent} from "../../Components/Books/cooking.component";

@NgModule({
    imports: [CommonModule],
    declarations: [CookingComponent],
    bootstrap: [CookingComponent]
})
export class CookingModule {
}