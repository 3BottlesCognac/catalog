﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {CookingComponent} from "../../Components/Books/cooking.component";
import {EsotericsComponent} from "../../Components/Books/esoterics.component";
import {ProgrammingComponent} from "../../Components/Books/programming.component";
import {BooksComponent} from "../../Components/Books/books.component";

@NgModule({
    imports: [CommonModule],
    declarations: [BooksComponent, CookingComponent, EsotericsComponent, ProgrammingComponent],
    bootstrap: [BooksComponent]
})
export class BooksModule {
}