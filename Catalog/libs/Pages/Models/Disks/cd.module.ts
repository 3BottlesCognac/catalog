﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BaseDiskModule} from "./base-disk.module";
import {MusicModule} from "./music.module";
import {VideoModule} from "./video.module";
import {SoftwareModule} from "./software.module";
import {CdComponent} from "../../Components/Disks/cd.component";

@NgModule({
    imports: [CommonModule],
    declarations: [CdComponent, MusicModule, VideoModule, SoftwareModule, BaseDiskModule],
    bootstrap: [CdComponent]
})
export class CdModule {
}