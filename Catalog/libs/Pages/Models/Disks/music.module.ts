﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MusicComponent} from "../../Components/Disks/music.component";
import {BaseDiskComponent} from "../../Components/Disks/base-disk.component";

@NgModule({
    imports: [CommonModule],
    declarations: [MusicComponent, BaseDiskComponent],
    bootstrap: [MusicComponent]
})
export class MusicModule {
}