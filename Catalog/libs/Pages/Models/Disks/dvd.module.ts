﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {DvdComponent} from "../../Components/Disks/dvd.component";
import {MusicComponent} from "../../Components/Disks/music.component";
import {VideoComponent} from "../../Components/Disks/video.component";
import {SoftwareComponent} from "../../Components/Disks/software.component";
import {BaseDiskComponent} from "../../Components/Disks/base-disk.component";

@NgModule({
    imports: [CommonModule],
    declarations: [DvdComponent, MusicComponent, VideoComponent, SoftwareComponent, BaseDiskComponent],
    bootstrap: [DvdComponent]
})
export class DvdModule {
}