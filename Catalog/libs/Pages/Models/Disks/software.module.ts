﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SoftwareComponent} from "../../Components/Disks/software.component";
import {BaseDiskComponent} from "../../Components/Disks/base-disk.component";

@NgModule({
    imports: [CommonModule],
    declarations: [SoftwareComponent, BaseDiskComponent],
    bootstrap: [SoftwareComponent]
})
export class SoftwareModule {
}