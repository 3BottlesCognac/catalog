﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {BaseDiskComponent} from "../../Components/Disks/base-disk.component";

@NgModule({
    imports: [CommonModule],
    declarations: [BaseDiskComponent],
    bootstrap: [BaseDiskComponent]
})
export class BaseDiskModule {
}