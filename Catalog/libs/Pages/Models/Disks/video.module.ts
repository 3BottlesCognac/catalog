﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {VideoComponent} from "../../Components/Disks/video.component";
import {BaseDiskComponent} from "../../Components/Disks/base-disk.component";

@NgModule({
    imports: [CommonModule],
    declarations: [VideoComponent, BaseDiskComponent],
    bootstrap: [VideoComponent]
})
export class VideoModule {
}