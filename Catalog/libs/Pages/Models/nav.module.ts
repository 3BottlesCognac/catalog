﻿import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {NavComponent} from "../Components/nav.component";

@NgModule({
    imports: [CommonModule],
    declarations: [NavComponent],
    bootstrap: [NavComponent]
})
export class NavModule {
}