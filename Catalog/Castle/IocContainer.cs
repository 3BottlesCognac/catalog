﻿using System.Web.Mvc;
using Backpack.Castle;
using Castle.Windsor;

namespace Backpack.Container
{
    public class IocContainer
    {
        private static IWindsorContainer _container;

        public static void Setup()
        {
            var container = new WindsorContainer();
            container.Install(new ApplicationCastleInstaller());

            var castleControllerFactory = new CastleControllerFactory(container);

            ControllerBuilder.Current.SetControllerFactory(castleControllerFactory);
        }

        public static void Dispose()
        {
            _container.Dispose();
        }
    }
}