﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Catalog.DomainServices;
using Catalog.DomainServices.Interfaces;
using Catalog.DomainServices.Interfaces.Books;
using Catalog.ViewModels;
using Catalog.ViewModels.Books;
using Catalog.ViewModels.Interfaces;

namespace Backpack.Castle
{
    public class ApplicationCastleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes
                .FromThisAssembly()
                .Pick().If(t => t.Name.EndsWith("Controller"))
                .Configure(configurer => configurer.Named(configurer.Implementation.Name))
                .LifestylePerWebRequest()
            );

            container.Register(Component.For<IProgrammingViewModel>().ImplementedBy<ProgrammingViewModel>());
            container.Register(Component.For<IProgrammingDomainService>().ImplementedBy<ProgrammingDomainService>());
            container.Register(Component.For<ICookingDomainService>().ImplementedBy<CookingDomainService>());
            container.Register(Component.For<ICookingViewModel>().ImplementedBy<CookingViewModel>());
            container.Register(Component.For<IEsotericsDomainService>().ImplementedBy<EsotericsDomainService>());
            container.Register(Component.For<IEsotericsViewModel>().ImplementedBy<EsotericsViewModel>());
            container.Register(Component.For<IDiskViewModel>().ImplementedBy<DiskViewModel>());
            container.Register(Component.For<IDiskDomainService>().ImplementedBy<DiskDomainService>());
        }
    }
}