﻿namespace Catalog.Entity.Iterfaces
{
    public interface IHasId
    {
        long Id { get; set; }
    }
}