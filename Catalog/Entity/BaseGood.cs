﻿using System.ComponentModel.DataAnnotations;

namespace Catalog.Entity
{
    /// <summary> Общие свойства товара </summary>
    public class BaseGood : BaseEntity
    {
        /// <summary> Стоимость товара</summary>
        [Display(Name = "Стоимость товара")]
        public string Cost { get; set; }

        /// <summary> Штрих-код  товара</summary>
        [Display(Name = "Штрих-код  товара")]
        public string Code { get; set; }
    }
}