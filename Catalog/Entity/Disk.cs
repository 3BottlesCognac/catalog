﻿using System.ComponentModel.DataAnnotations;
using Catalog.Enums;

namespace Catalog.Entity
{
    /// <summary> Cвойства дисков </summary>
    public class Disk : BaseGood
    {
        /// <summary> Тип диска</summary>
        [Display(Name = "Тип диска")]
        public EDisk TypeDisk { get; set; }

        /// <summary> Содержимое диска</summary>
        [Display(Name = "Содержимое диска")]
        public EDickContent Content { get; set; }
    }
}