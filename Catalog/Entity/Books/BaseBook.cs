﻿using System.ComponentModel.DataAnnotations;
using Catalog.Enums;

namespace Catalog.Entity.Books
{
    /// <summary>  Общие свойства книг </summary>
    public class BaseBook : BaseGood
    {
        /// <summary> Число страниц</summary>
        [Display(Name = "Число страниц")]
        public int NumberPages { get; set; }
    }
}