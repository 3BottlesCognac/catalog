﻿using System.ComponentModel.DataAnnotations;

namespace Catalog.Entity.Books
{
    /// <summary> Сущность книг по кулинарии   </summary>
    public class Cooking : BaseBook
    {
        /// <summary> Основной ингредиент  </summary>
        [Display(Name = "Основной ингредиент ")]
        public string BasicIngredient { get; set; }
    }
}