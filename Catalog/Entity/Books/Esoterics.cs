﻿using System.ComponentModel.DataAnnotations;

namespace Catalog.Entity.Books
{
    /// <summary> Сущность книг по эзотерике  </summary>
    public class Esoterics : BaseBook
    {
        /// <summary> Минимальный возраст читателя </summary>
        [Display(Name = "Минимальный возраст читателя")]
        public int MinAge { get; set; }
    }
}