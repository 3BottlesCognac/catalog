﻿using System.ComponentModel.DataAnnotations;

namespace Catalog.Entity.Books
{
    /// <summary> Сущность книг по программированию </summary>
    public class Programming : BaseBook
    {
        /// <summary> Язык программирования</summary>
        [Display(Name = "Язык программирования")]
        public string Language { get; set; }
    }
}