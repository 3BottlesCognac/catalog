﻿using System.ComponentModel.DataAnnotations;
using Catalog.Entity.Iterfaces;

namespace Catalog.Entity
{
    /// <summary> Общие свойства сущностей </summary>
    public class BaseEntity : IHasId
    {
        /// <summary> Полное наименование </summary>
        [Display(Name = "Название")]
        public string Name { get; set; }

        /// <summary> Идентификатор </summary>
        public long Id { get; set; }
    }
}