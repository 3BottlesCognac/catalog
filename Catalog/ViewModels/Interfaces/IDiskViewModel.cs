﻿using System.Collections.Generic;
using Catalog.Dtos;
using Catalog.Enums;

namespace Catalog.ViewModels.Interfaces
{
    public interface IDiskViewModel
    {
        List<DiskDto> List(EDisk typeDisk, EDickContent content);
    }
}