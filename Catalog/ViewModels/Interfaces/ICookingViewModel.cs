﻿using System.Collections.Generic;
using Catalog.Dtos.Books;
using Catalog.Enums;

namespace Catalog.ViewModels.Interfaces
{
    public interface ICookingViewModel
    {
        List<CookingDto> List();
    }
}