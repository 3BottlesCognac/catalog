﻿using System.Collections.Generic;
using Catalog.Dtos.Books;
using Catalog.Enums;

namespace Catalog.ViewModels.Interfaces
{
    public interface IProgrammingViewModel
    {
        List<ProgrammingDto> List();
    }
}