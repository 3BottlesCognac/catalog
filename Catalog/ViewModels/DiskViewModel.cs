﻿using System.Collections.Generic;
using System.Linq;
using Catalog.DomainServices.Interfaces;
using Catalog.Dtos;
using Catalog.Enums;
using Catalog.ViewModels.Interfaces;

namespace Catalog.ViewModels
{
    public class DiskViewModel : IDiskViewModel
    {
        public IDiskDomainService DiskDomainService { get; set; }

        public List<DiskDto> List(EDisk typeDisk, EDickContent content)
        {
            var list = DiskDomainService.GetAll().Where(disk => disk.TypeDisk == typeDisk && disk.Content == content)
                .Select(book =>
                    new DiskDto
                    {
                        Code = book.Code,
                        Cost = book.Cost,
                        Name = book.Name
                    }).ToList();

            return list;
        }
    }
}