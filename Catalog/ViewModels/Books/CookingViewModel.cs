﻿using System.Collections.Generic;
using System.Linq;
using Catalog.DomainServices.Interfaces.Books;
using Catalog.Dtos.Books;
using Catalog.Enums;
using Catalog.ViewModels.Interfaces;

namespace Catalog.ViewModels.Books
{
    public class CookingViewModel : ICookingViewModel
    {
        public ICookingDomainService CookingDomainService { get; set; }

        public List<CookingDto> List()
        {
            var list = CookingDomainService.GetAll().Select(book =>
                new CookingDto
                {
                    Code = book.Code,
                    Cost = book.Cost,
                    Name = book.Name,
                    NumberPages = book.NumberPages,
                    BasicIngredient = book.BasicIngredient
                }).ToList();

            return list;
        }
    }
}