﻿using System.Collections.Generic;
using System.Linq;
using Catalog.DomainServices.Interfaces.Books;
using Catalog.Dtos.Books;
using Catalog.Enums;
using Catalog.ViewModels.Interfaces;

namespace Catalog.ViewModels.Books
{
    public class EsotericsViewModel : IEsotericsViewModel
    {
        public IEsotericsDomainService EsotericsDomainService { get; set; }

        public List<EsotericsDto> List()
        {
            var list = EsotericsDomainService.GetAll().Select(book =>
                new EsotericsDto
                {
                    Code = book.Code,
                    Cost = book.Cost,
                    Name = book.Name,
                    NumberPages = book.NumberPages,
                    MinAge = book.MinAge
                }).ToList();

            return list;
        }
    }
}