﻿using System.Collections.Generic;
using System.Linq;
using Catalog.DomainServices.Interfaces.Books;
using Catalog.Dtos.Books;
using Catalog.Enums;
using Catalog.ViewModels.Interfaces;

namespace Catalog.ViewModels.Books
{
    public class ProgrammingViewModel : IProgrammingViewModel
    {
        public IProgrammingDomainService ProgrammingDomainService { get; set; }

        public List<ProgrammingDto> List()
        {
            var list = ProgrammingDomainService.GetAll().Select(book =>
                new ProgrammingDto
                {
                    Code = book.Code,
                    Cost = book.Cost,
                    Language = book.Language,
                    Name = book.Name,
                    NumberPages = book.NumberPages
                }).ToList();

            return list;
        }
    }
}