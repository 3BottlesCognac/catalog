﻿namespace Catalog.Dtos
{
    /// <summary> Дто товаров</summary>
    public class BaseGoodsDto
    {
        /// <summary> Цена товара</summary>
        public string Name { get; set; }

        /// <summary> Стоимость товара</summary>
        public string Cost { get; set; }

        /// <summary> Штрих-код  товара</summary>
        public string Code { get; set; }
    }
}