﻿namespace Catalog.Dtos.Books
{
    /// <summary> Дто книг по программированию </summary>
    public sealed class ProgrammingDto : BaseBookDto
    {
        /// <summary> Язык программирования</summary>
        public string Language { get; set; }
    }
}