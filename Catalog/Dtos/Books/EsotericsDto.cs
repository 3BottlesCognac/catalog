﻿namespace Catalog.Dtos.Books
{
    /// <summary> Дто книг по эзотерике  </summary>
    public sealed class EsotericsDto : BaseBookDto
    {
        /// <summary> Минимальный возраст читателя </summary>
        public int MinAge { get; set; }
    }
}