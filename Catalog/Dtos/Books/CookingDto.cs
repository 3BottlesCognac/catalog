﻿namespace Catalog.Dtos.Books
{
    /// <summary> Дто книг по кулинарии  </summary>
    public sealed class CookingDto : BaseBookDto
    {
        /// <summary> Основной ингредиент </summary>
        public string BasicIngredient { get; set; }
    }
}