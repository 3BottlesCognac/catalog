﻿namespace Catalog.Dtos.Books
{
    /// <summary> Дто книг</summary>
    public class BaseBookDto : BaseGoodsDto
    {
        /// <summary> Число страниц</summary>
        public int NumberPages { get; set; }
    }
}