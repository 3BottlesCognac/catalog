﻿using System.Data.Entity;
using Catalog.Entity;
using Catalog.Entity.Books;

namespace Catalog
{
    public class DBContext : DbContext
    {
        public DBContext() : base("CatalogDB")
        {
        }

        public DbSet<Programming> Programmings { get; set; }
        public DbSet<Cooking> Cookings { get; set; }
        public DbSet<Esoterics> Esotericses { get; set; }
        public DbSet<Disk> Disks { get; set; }
    }
}