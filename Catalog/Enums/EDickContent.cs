﻿using System.ComponentModel.DataAnnotations;

namespace Catalog.Enums
{
    /// <summary>Программирование</summary>
    public enum EDickContent
    {
        /// <summary>Музыка</summary>
        [Display(Name = "Музыка")] Music = 5,

        /// <summary>Видео</summary>
        [Display(Name = "Видео")] Video = 10,

        /// <summary>ПО</summary>
        [Display(Name = "ПО")] Software = 15
    }
}