﻿using System.ComponentModel.DataAnnotations;

namespace Catalog.Enums
{
    /// <summary>Виды дисков</summary>
    public enum EDisk
    {
        /// <summary>CD</summary>
        [Display(Name = "CD")] CD = 1,

        /// <summary>DVD</summary>
        [Display(Name = "DVD")] DVD = 2
    }
}