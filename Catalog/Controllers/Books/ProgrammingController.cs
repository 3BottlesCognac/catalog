﻿using System.Web.Mvc;
using Catalog.DomainServices;
using Catalog.Entity.Books;
using Catalog.Enums;
using Catalog.ViewModels.Interfaces;

namespace Catalog.Controllers.Books
{
    public class ProgrammingController : BaseController<ProgrammingDomainService, Programming>
    {
        public IProgrammingViewModel ProgrammingViewModel { get; set; }

        public JsonResult List()
        {
            var list = ProgrammingViewModel.List();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}