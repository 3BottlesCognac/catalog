﻿using System.Web.Mvc;
using Catalog.DomainServices;
using Catalog.Entity.Books;
using Catalog.ViewModels.Interfaces;

namespace Catalog.Controllers.Books
{
    public class CookingController : BaseController<CookingDomainService, Cooking>
    {
        public ICookingViewModel CookingViewModel { get; set; }

        public JsonResult List()
        {
            var list = CookingViewModel.List();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}