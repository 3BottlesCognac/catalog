﻿using System.Web.Mvc;
using Catalog.DomainServices;
using Catalog.Entity.Books;
using Catalog.Enums;
using Catalog.ViewModels.Interfaces;

namespace Catalog.Controllers.Books
{
    public class EsotericsController : BaseController<EsotericsDomainService, Esoterics>
    {
        public IEsotericsViewModel EsotericsViewModel { get; set; }

        public JsonResult List()
        {
            var list = EsotericsViewModel.List();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}