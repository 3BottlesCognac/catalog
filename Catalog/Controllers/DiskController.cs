﻿using System.Web.Mvc;
using Catalog.DomainServices;
using Catalog.Entity;
using Catalog.Enums;
using Catalog.ViewModels.Interfaces;

namespace Catalog.Controllers
{
    public class DiskController : BaseController<DiskDomainService, Disk>
    {
        public IDiskViewModel DiskViewModel { get; set; }

        public JsonResult List(EDisk typeDisk, EDickContent content)
        {
            var list = DiskViewModel.List(typeDisk, content);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}