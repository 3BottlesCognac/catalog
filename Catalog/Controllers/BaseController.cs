﻿using System.Web.Mvc;
using Backpack.DomainServices.Interfaces;

namespace Catalog.Controllers
{
    public class BaseController<TDomainService, TEntity> : Controller
        where TEntity : class
        where TDomainService : IBaseDomainService<TEntity>, new()
    {
        [HttpPost]
        public void Remove(long id)
        {
            new TDomainService().Remove(id);
        }

        [HttpPost]
        public void Edit(TEntity entity)
        {
            new TDomainService().Edit(entity);
        }

        [HttpPost]
        public void Create(TEntity entity)
        {
            new TDomainService().Save(entity);
        }
    }
}